import React from 'react'
import { Link } from 'react-router-dom'
import ROUTES from '../routes'

import customersService from './customers-service'

import Collapsible from '../collapsible'

class CustomerDetail extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      customer: {}
    }
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
  }

  componentWillReceiveProps(newProps) {
    if (this.props.match.params.id !== newProps.match.params.id) {
      this.load(newProps.match.params.id)
    }
  }

  async load(id) {
    const res = await customersService.getCustomer(id)

    this.setState({
      customer: res.data
    })
  }

  render() {
    const customer = this.state.customer

    return (
      
      <div className="box box-default">
        <div className="box-header">
          <h3 className="box-title">Customer Detail</h3>
        </div>
        <div className="box-body">
          <div className="col-md-12">
            <div className="btn-group margin-bottom">
              <Link className="btn btn-default" to={ROUTES.getUrl(ROUTES.CUSTOMER_EDIT, { id: customer.id })}>Edit</Link>
              <Link className="btn btn-default" to={ROUTES.getUrl(ROUTES.CUSTOMER_LISTING)}>Delete</Link>
            </div>
            <table className="table table-borderless margin-bottom">
              <tbody>
                <tr>
                  <th className="col-md-1 text-right">Name</th>
                  <td className="col-md-11">{customer.name}</td>
                </tr>
                <tr>
                  <th className="text-right">E-mail</th>
                  <td>{customer.email}</td>
                </tr>
                <tr>
                  <th className="text-right">Phone</th>
                  <td>{customer.phone}</td>
                </tr>
                <tr>
                  <th className="text-right">City</th>
                  <td>{customer.city}</td>
                </tr>
                <tr>
                  <th className="text-right">Note</th>
                  <td>
                    <Collapsible>
                      {customer.note}
                    </Collapsible>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>  
      </div>
    )
  }
}

export default CustomerDetail
