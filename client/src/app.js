import React from "react"

import { HashRouter, Route, Switch, NavLink, Link } from "react-router-dom"

import authService from "./auth-service"

import Login from "./login"

import Dashboard from "./dashboard/dashboard"

import UserListing from "./users/user-listing"
import UserEdit from "./users/user-edit"
import UserNew from "./users/user-new"
import UserDetail from "./users/user-detail"
import UserPasswordChange from "./users/user-password-change"
import RecoverPassword from "./users/user-password-recover"

import CustomerListing from "./customers/customer-listing"
import CustomerEdit from "./customers/customer-edit"
import CustomerNew from "./customers/customer-new"
import CustomerDetail from "./customers/customer-detail"

import ContractListing from "./contracts/contracts-listing"
import ContractEdit from "./contracts/contract-edit"
import ContractNew from "./contracts/contract-new"
import ContractDetail from "./contracts/contract-detail"

import AuditLog from "./audit-log/audit-log-listing"

import ROUTES from "./routes"

const App = () => (
  <HashRouter>
    <div className="router-rubbish">
      <Switch>
        <Route
          path={ROUTES.USER_PASSWORD_RECOVER}
          component={RecoverPassword}
        />
        <Route
          path="/"
          render={() => (
            <React.Fragment>
              {!authService.isLoggedIn() && <Login />}
              {authService.isLoggedIn() && (
                <div className="router-rubbish">
                  <Navigation brand="CustomersApp" />
                  <Sidebar />
                  <div className="content-wrapper">
                    <div className="content-container">
                      <Switch>
                        <Route path={ROUTES.DASHBOARD} component={Dashboard} />

                        {/* Users */}
                        <Route
                          path={ROUTES.USER_DETAIL}
                          component={UserDetail}
                        />
                        <Route path={ROUTES.USER_EDIT} component={UserEdit} />
                        <Route path={ROUTES.USER_NEW} component={UserNew} />
                        <Route
                          path={ROUTES.USER_LISTING}
                          component={UserListing}
                        />
                        <Route
                          path={ROUTES.USER_PASSWORD_CHANGE}
                          component={UserPasswordChange}
                        />

                        {/* Customers */}
                        <Route
                          path={ROUTES.CUSTOMER_DETAIL}
                          component={CustomerDetail}
                        />
                        <Route
                          path={ROUTES.CUSTOMER_EDIT}
                          component={CustomerEdit}
                        />
                        <Route
                          path={ROUTES.CUSTOMER_NEW}
                          component={CustomerNew}
                        />
                        <Route
                          path={ROUTES.CUSTOMER_LISTING}
                          component={CustomerListing}
                        />

                        {/* Contracts */}
                        <Route
                          path={ROUTES.CONTRACT_DETAIL}
                          component={ContractDetail}
                        />
                        <Route
                          path={ROUTES.CONTRACT_NEW}
                          component={ContractNew}
                        />
                        <Route
                          path={ROUTES.CONTRACT_EDIT}
                          component={ContractEdit}
                        />
                        <Route
                          path={ROUTES.CONTRACT_LISTING}
                          component={ContractListing}
                        />

                        {/* Audit log */}
                        <Route path={ROUTES.AUDIT_LOG} component={AuditLog} />

                        <Route path="/" component={ContractListing} />
                      </Switch>
                    </div>
                  </div>
                </div>
              )}
            </React.Fragment>
          )}
        />
      </Switch>
    </div>
  </HashRouter>
)

const Navigation = props => (
  <header className="main-header">
    <Link to={ROUTES.CONTRACT_LISTING} className="logo">
      <span className="logo-mini">
        <b>C</b>RM
      </span>
      <span className="logo-lg">
        <b>{props.brand}</b>
      </span>
    </Link>
    <nav className="navbar navbar-static-top">
      <a
        href=""
        className="sidebar-toggle"
        data-toggle="push-menu"
        role="button"
      >
        <span className="sr-only">Toggle navigation</span>
      </a>

      <div className="navbar-custom-menu">
        <ul className="nav navbar-nav">
          <li className="dropdown user user-menu">
            <a
              href=""
              className="dropdown-toggle"
              data-toggle="dropdown"
              aria-expanded="false"
            >
              <img
                src="user1-128x128.jpg"
                className="user-image"
                alt="User Pic"
              />
              <span className="hidden-xs">{authService.user.name}</span>
            </a>
            <ul className="dropdown-menu">
              <li className="user-header">
                <img
                  src="user1-128x128.jpg"
                  className="img-circle"
                  alt="User Pic"
                />
                <p>
                  {authService.user.name} - {authService.user.username}
                  <small>Member since Nov. 2012</small>
                </p>
              </li>

              <li className="user-body">
                <div className="row">
                  <div className="col-xs-4 text-center">
                    <a href="">Followers</a>
                  </div>
                  <div className="col-xs-4 text-center">
                    <a href="">Sales</a>
                  </div>
                  <div className="col-xs-4 text-center">
                    <a href="">Friends</a>
                  </div>
                </div>
              </li>

              <li className="user-footer">
                <div className="pull-left">
                  <Link
                    to={ROUTES.USER_PASSWORD_CHANGE}
                    className="btn btn-default btn-flat"
                  >
                    Change password
                  </Link>
                </div>
                <div className="pull-right">
                  <a
                    href="#/"
                    className="btn btn-default btn-flat"
                    onClick={() => authService.logout()}
                  >
                    Sign out
                  </a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
)

const Sidebar = () => (
  <aside className="main-sidebar">
    <section className="sidebar" style={{ height: "auto" }}>
      <div className="user-panel">
        <div className="pull-left image">
          <img src="user1-128x128.jpg" className="img-circle" alt="User Pic" />
        </div>
        <div className="pull-left info">
          <p>{authService.user.name}</p>
          <a href="">
            <i className="fa fa-circle text-success" /> Online
          </a>
        </div>
      </div>

      <ul className="sidebar-menu tree" data-widget="tree">
        <li className="header">MAIN NAVIGATION</li>
        <li>
          <NavLink to={ROUTES.DASHBOARD}>
            <i className="fa fa-dashboard" /> <span>Dashboard</span>
          </NavLink>
        </li>
        <li>
          <NavLink to={ROUTES.CONTRACT_LISTING}>
            <i className="fa fa-th" /> <span>Contracts</span>
          </NavLink>
        </li>
        <li>
          <NavLink to={ROUTES.CUSTOMER_LISTING}>
            <i className="fa fa-th" /> <span>Customers</span>
          </NavLink>
        </li>
        <li>
          <NavLink to={ROUTES.USER_LISTING}>
            <i className="fa fa-laptop" /> <span>Users</span>
          </NavLink>
        </li>
        <li>
          <NavLink to={ROUTES.AUDIT_LOG}>
            <i className="fa fa-laptop" /> <span>Audit log</span>
          </NavLink>
        </li>
      </ul>
    </section>
  </aside>
)

export default App
