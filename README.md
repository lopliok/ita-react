This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Release document.
https://docs.google.com/document/d/1wq0zvAVvONy8HHS6ckVGCCo0h70i4FGZHaCUA8Z_W44/edit

<img src='https://g.gravizo.com/svg?
@startuml;

actor User;
participant "First Class" as A;
participant "Second Class" as B;
participant "Last Class" as C;

User -> A: DoWork;
activate A;

A -> B: Create Request;
activate B;

B -> C: DoWork;
activate C;

C --> B: WorkDone;
destroy C;

B --> A: Request Created;
deactivate B;

A --> User: Done;
deactivate A;

@enduml
'>


```plantuml
@startuml
skinparam monochrome true
skinparam ParticipantPadding 20
skinparam BoxPadding 30


actor User
box "Localhost"
participant Client
participant Backend
end box
participant Gateway

User -> Client: Open web app 
activate Client 
Client -> Backend: GET: getSchedule()   
activate Backend
Backend --> Client: OK: 200
deactivate Backend

Client -> Client: groupSchedule()
Client --> User: viewPerformance
deactivate Client

User -> Client: Invalidate Performance
activate Client
Client -> Gateway: POST: invalidateSchedule()   
activate Gateway
Gateway --> Client: OK: 200
deactivate Gateway
deactivate Client

@enduml

```